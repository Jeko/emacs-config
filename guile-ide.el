;; (require 'package)
;; (setq package-archives '(("melpa" . "https://melpa.org/packages/")))

;; Ajouter le dossier site-lisp au load-path.
;; (add-to-list 'load-path "~/.emacs.d/site-lisp/")

;; (setq use-package-always-ensure t)

(use-package emacs
  :bind ("C-c x" . export-current-symbol)
  :hook ((c-mode . electric-pair-mode))
  :config
  (global-hl-line-mode +1)
  (show-paren-mode 1)
  (setq confirm-kill-processes nil))

;; Skribilo
;; Minor mode automatique quand on édite un fichier .skr ou .skb
(use-package skribe-mode
  :init
  (autoload 'skribe-mode "skribe.el" "Skribe mode." t)
  :mode
  ("\\.skr\\'" "\\.skb\\'")
  :hook
  ((skribe-mode-hook . turn-on-auto-fill)))

;; VCS
(use-package magit
  :custom
  (magit-auto-revert-mode 1)
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;; Direnv
(use-package envrc
  :config
  (envrc-global-mode))

;; Guile (Scheme)
;; Must-have

(use-package paren-face
  :custom (paren-face-regexp "#?[](){}[]")
  :config (global-paren-face-mode 1))

(use-package geiser-guile
  :custom
  (geiser-default-implementation 'guile)
  (geiser-active-implementations '(guile))
  (geiser-repl-per-project-p t)
  (geiser-implementations-alist '(((regexp "\\.scm$") guile)))
  (geiser-guile-manual-lookup-nodes '("guile"
				      "guix"))
  ;; https://github.com/mrvdb/emacs-config
  ;; Do NOT evaluate on return when inside an expression  (geiser-repl-send-on-return-p nil)
  (geiser-repl-send-on-return-p nil)
  ;; start repl and eval results inline in buffer
  (geiser-mode-start-repl-p t)
  ;; TODO I do want this eval result to be transient, not inserted!
  (geiser-mode-eval-last-sexp-to-buffer t)
  (geiser-mode-eval-to-buffer-prefix " ;;=> "))

(use-package eros
  :config
  (eros-mode 1))

(use-package geiser-eros
  ;; https://github.com/mrvdb/emacs-config
  :after (eros geiser)
  :config
  ;; Make sure geiser does not insert eval into buffer
  (setq geiser-mode-eval-last-sexp-to-buffer nil)
  (geiser-eros-mode 1))

(use-package geiser-gunit64
  :after (geiser)
  :hook ((geiser-mode . geiser-gunit64-mode)))

(use-package paredit
  :hook ((emacs-lisp-mode . enable-paredit-mode)
	 (lisp-interaction-mode . enable-paredit-mode)
	 (lisp-mode . enable-paredit-mode)
	 (scheme-mode . enable-paredit-mode)
	 (geiser-repl-mode . enable-paredit-mode)
	 (skribe-mode . enable-paredit-mode))
  :config
  (add-hook 'eval-expression-minibuffer-setup-hook #'paredit-mode)
  ;; Thanks to Maxim Cournoyer for the fix https://lists.gnu.org/archive/html/help-guix/2023-01/msg00112.html
  (define-key paredit-mode-map (kbd "RET") nil)              ;; fix non returning REPL prompt
  (define-key paredit-mode-map (kbd "C-j") 'paredit-newline) ;; fix non returning REPL prompt
  )

(use-package aggressive-indent
  :hook ((prog-mode . aggressive-indent-mode)))

(use-package flycheck
  :init
  (global-flycheck-mode 1))

(use-package flycheck-guile)

(use-package web-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.html\\.tpl\\'" . web-mode))
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2))

;; Indentation pour les macros de Guile-Spec
(defun my-scheme-mode-hook ()
  "Customize some macro indentation."
  (put 'describe 'scheme-indent-function 1)
  (put 'context 'scheme-indent-function 1)
  (put 'it 'scheme-indent-function 1)
  (put 'should 'scheme-indent-function 1)
  (put 'should= 'scheme-indent-function 1)
  (put 'should-throw 'scheme-indent-function 1)
  (put 'test-error 'scheme-indent-function 2))
(add-hook 'scheme-mode-hook 'my-scheme-mode-hook)

;; Activer le mode Scheme pour les fichiers .guile ou .scm
(add-to-list 'auto-mode-alist '("\\.guile\\'" . scheme-mode))
(add-to-list 'auto-mode-alist '("\\.scm\\'" . scheme-mode))

;; Recherche et remplace
(use-package iedit
  :bind (("C-;" . iedit-mode)))

;; Edition
(use-package multiple-cursors
  :bind ("C-c m c" . 'mc/edit-lines))

;; Pair-programming
;; Lockstep pour suivre le conducteur de la session de pair-programming.
;; (require 'lockstep)
;; (global-set-key (kbd "C-c s e") 'turn-on-lockstep)
;; (global-set-key (kbd "C-c s d") 'turn-off-lockstep)

;; Une couleur par curseur.
;; (eval-and-compile 
;;   (load (expand-file-name "site-lisp/colorful-points.el" user-emacs-directory)))
;; (global-set-key (kbd "C-c c") 'colorful-points-mode)

;; TDD and TCR
;; Met à jour automatiquement le buffer et Dired quand un fichier est modifié par un programme externe.
(global-auto-revert-mode 1)
(add-hook 'dired-mode-hook 'auto-revert-mode)

;; Emacs in terminal
;; Add space between line numbers and buffer content.
;; (setq linum-format "%4d \u2502 ")
(if (not (display-graphic-p))
    (setq linum-format
          (lambda (line)
            (propertize (format (let ((w (length (number-to-string (count-lines (point-min) (point-max))))))
                                  (concat "%" (number-to-string w) "d \u2502 ")) line) 'face 'linum))))

(add-hook 'scheme-mode-hook 'guix-devel-mode)

;; Guile tooling
(defun export-current-symbol ()
  "Add the symbol at point to the list of exported symbols"
  (interactive)
  (let ((symbol (thing-at-point 'symbol t)))
    (save-excursion
      (search-backward "#:export" nil t)
      (forward-list)
      (backward-char)
      (if (thing-at-point 'symbol)
	  (newline))
      (insert symbol))))


